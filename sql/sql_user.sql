-- Drop Database if exist
DROP DATABASE `supermarket_online`;

-- Create Database
CREATE DATABASE `supermarket_online`;

-- Create user table
CREATE TABLE supermarket_online.user (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `varsta` int NOT NULL,
  `oras` varchar(100) NOT NULL,
  `rol` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


-- Insert default value for users
insert into supermarket_online.user values (1, 'alexgeorge', 'alexgeorge', 21, 'Bucuresti','ADMIN');
insert into supermarket_online.user values (2, 'andrei', 'admin21', 29, 'Bucuresti','ADMIN');
insert into supermarket_online.user values (3, 'Ionut', 'fotball', 31, 'Bucuresti','USER');
insert into supermarket_online.user values (4, 'test', 'vreme', 45, 'Bucuresti','USER');


-- create table product
CREATE TABLE supermarket_online.product (
  `id` int NOT NULL AUTO_INCREMENT,
  `nume` varchar(100) NOT NULL,
  `cantitate` float NOT NULL,
  `data_exp` varchar(100) NOT NULL,
  `pret` float NOT NULL,
  `categorie` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO supermarket_online.product VALUES (1,'cartofi',1000,'31.12.2022',3.5,'legume');
INSERT INTO supermarket_online.product VALUES (2,'iaurt Danone 140 g',550,'30.07.2022',2,'lactate');
INSERT INTO supermarket_online.product VALUES (3,'Pepsi Twist 2 L',200,'31.10.2022',7,'bauturi');
INSERT INTO supermarket_online.product VALUES (4,'Ciocolata Heidi',200,'31.10.2022',8,'dulciuri');

commit;

