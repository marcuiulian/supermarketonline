# SupermarketOnline



## Link Task-uri GoogleSheets

- https://docs.google.com/spreadsheets/d/1D03HJckfvRSRNOgw6ARxVrV6w0pQLKDgwYcq1OWXZ64/edit?usp=sharing

Plan Supermarket online

##	Documentatie: ce inseamna supermarket online
##	Ce tooluri vom folosi: Intellij – Hibernate; Maven; Junit; Mokito / MySql Workbach / Git Lab / Google sheets
##	Definire scopuri:
      a.	Vanzarea produselor catre client
      b.	Livrarea produselor catre client (optional)
      c.	Istoric comenzi pentru utilizatori
      d.	Returnarea unui produs
##	Definirea functionalitati:
      a.	Adaugare produse (create)
      b.	Modificare detalii produse (update)
      c.	Stergerea unui produs (delete)
      d.	Afisare produse (read)
      e.	Adaugare utilizatori (username/parola)
      f.	Crearea unui cos de cumparaturi per user
      g.	Vizualizare istoric pentru client
      h.	Crearea unui formular de retur
      i.	Adaugarea unui formilar de feedback
      j.	Procesarea plati (optional)
##	Arhitectura aplicatiei:
      a.	Arhitectura aplicatiei java
      i.	Configurarea proiectului
      ii.	Structura pachetelor
      b.	Arhitectura bazei de date
      i.	Definirea tabelelor
      ii.	Definirea legaturilor dintre tabele


##	Definirea taskurilor
##	Implementarea taskurilor
##	Testarea aplicatiei: manual / unit testing / integration testing
##	Documentatia tehnica


## Link for hibernate sql
-   https://www.tutorialspoint.com/hibernate/hibernate_query_language.htm

-   https://docs.jboss.org/hibernate/core/3.3/reference/en/html/queryhql.html

-   https://www.javatpoint.com/hql


## Future functionalities

- Create a new method for feedback (create addition table to store this date for every request)
- Create e method to see shopping history for current user (check if the user had previous request)
- Refactor code to be more 'visible'
- Move all methods regarding products to a service called ProductService, same for user
- Create UNIT TESTING methods!