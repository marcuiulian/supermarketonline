package mapper;

import dto.UserDTO;
import entities.User;

public class UserMapper {

    public static UserDTO mapEntityToDto(User entityUser) {
        if (null == entityUser) {
            return null;
        }

        UserDTO userDTO = new UserDTO();

        userDTO.setId(entityUser.getId());
        userDTO.setOras(entityUser.getOras());
        userDTO.setUsername(entityUser.getUsername());
        userDTO.setPassword(entityUser.getPassword());
        userDTO.setRol(entityUser.getRol());
        userDTO.setVarsta(entityUser.getVarsta());

        return userDTO;
    }

    public static User mapDtoToEntity(UserDTO userDTO) {
        if (null == userDTO) {
            return null;
        }

        User userEntity = new User();

        userEntity.setId(userDTO.getId());
        userEntity.setOras(userDTO.getOras());
        userEntity.setUsername(userDTO.getUsername());
        userEntity.setPassword(userDTO.getPassword());
        userEntity.setRol(userDTO.getRol());
        userEntity.setVarsta(userDTO.getVarsta());

        return userEntity;
    }
}
