package mapper;

import dto.ProductDTO;
import entities.Product;

public class ProductMapper {
    
    public static ProductDTO mapEntityToDto(Product entity) {
        if (null == entity) {
            return null;
        }
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(entity.getId());
        productDTO.setProductCategory(entity.getProductCategory());
        productDTO.setPrice(entity.getPrice());
        productDTO.setExpiryDate(entity.getExpiryDate());
        productDTO.setQuantity(entity.getQuantity());
        return productDTO;
    }
    
    public static Product mapDtoToEntity(ProductDTO productDTO) {
        if (null == productDTO) {
            return null;
        }
        Product productEntity = new Product();
        productEntity.setId(productEntity.getId());
        productEntity.setProductCategory(productEntity.getProductCategory());
        productEntity.setPrice(productEntity.getPrice());
        productEntity.setQuantity(productEntity.getQuantity());
        productEntity.setExpiryDate(productDTO.getExpiryDate());
        return productEntity;
    }
    
}
