import config.UserSession;
import dto.ProductDTO;
import dto.UserDTO;
import dto.UserProduct;
import entities.Product;
import entities.User;
import enums.ProductCategory;
import enums.UserRol;
import mapper.ProductMapper;
import mapper.UserMapper;
import repository.ProductRepository;
import repository.ProductRepositoryImpl;
import repository.UserRepository;
import repository.UserRepositoryImpl;
import utils.ScannerHelper;

import java.util.*;

public class Application {
    public static void main(String[] args) {

        System.out.println("Welcome to supermarket online!");
        enterToShop();
    }

    private static void enterToShop() {
        int userDecision = getUserDecision();

        if (userDecision == 1) {
            if (isExistingUser()) {
                System.out.println("Succes");
                actionsForSuccessfullyAuthenticatedUser();
            } else {
                enterToShop();
            }
        } else if (userDecision == 2) {
            System.out.println("Creare cont nou");
            createNewAccount();
            enterToShop();
        }
    }

    private static void actionsForSuccessfullyAuthenticatedUser() {
        displayMenu();
        int loggedUserDecision = getLoggedUserDecision();

        switch (loggedUserDecision) {
            case 1:
                menuDecisionOneFindAllProducts();
                break;
            case 2:
                menuDecisionTwoFindProductByName();
                break;
            case 3:
                menuDecisionThreeUpdateProduct();
                break;
            case 4:
                menuDecisionFourDeleteProduct();
                break;
            case 5:
                menuDecisionFiveCreateNewProduct();
                break;
            case 6:
                menuDecisionSixFindProductByPrice();
                break;
            case 7:
                logoutUser();
                break;
            case 0:
                displayMenu();
                break;
        }
    }

    private static void logoutUser() {
        // todo 1 get user from session
        // todo 2 set user from session user.logged = false
        // todo 3 set user from session = null
        // todo 4 return to main menu
    }

    private static void menuDecisionSixFindProductByPrice() {
        Scanner scanner = ScannerHelper.getScanner();
        scanner.nextLine();
        System.out.println("Introduceti pretul produsului");
        float priceProduct = scanner.nextFloat();

        ProductRepositoryImpl productsFromDB = ProductRepositoryImpl.getInstance();
        Optional<List<Product>> foundProductList = productsFromDB.findByMinPrice(priceProduct);

        if (foundProductList.isPresent()) {
            System.out.println(foundProductList.get());
            System.out.println("Produsele au fost gasite, revenire la meniul principal..");
        } else {
            System.out.println("Product not found... redirecting to main menu");
        }
        actionsForSuccessfullyAuthenticatedUser();
    }


    private static void menuDecisionFiveCreateNewProduct() {
        Scanner scanner = ScannerHelper.getScanner();
        Product product = new Product();

        System.out.println("Introduceti numele produsului");
        product.setName(scanner.nextLine());

        System.out.println("Introduceti cantitatea produsului");
        product.setQuantity(scanner.nextFloat());

        System.out.println("Introduceti data expirarii a produsului");
        product.setExpiryDate(scanner.nextLine());

        System.out.println("Introduceti pretul produsului");
        product.setPrice(scanner.nextFloat());

        System.out.println("Introduceti categoria produsului: legume/lactate/bauturi/dulciuri/cafea/parfumuri");
        product.setProductCategory(ProductCategory.valueOf(scanner.nextLine()));

        ProductRepositoryImpl productsFromDB = ProductRepositoryImpl.getInstance();
        productsFromDB.create(product);
        System.out.println("Produsul a fost creat, revenire la meniul principal..");


        actionsForSuccessfullyAuthenticatedUser();
    }

    private static void menuDecisionFourDeleteProduct() {
        Scanner scanner = ScannerHelper.getScanner();

        System.out.println(ProductRepositoryImpl.getInstance().findAll());
        System.out.println("Introdu id-ul produsului");
        int idToBeDeleted = scanner.nextInt();

        System.out.println("Sunteti sigur ca doriti sa stergeti produsul cu id: " + idToBeDeleted);
        System.out.println("1 = DA");
        System.out.println("2 = NU");

        int decision = scanner.nextInt();

        if (decision == 1) {
            Optional<Product> foundProduct = ProductRepositoryImpl.getInstance().findById(idToBeDeleted);


            if (foundProduct.isPresent()) {
                ProductRepositoryImpl.getInstance().delete(foundProduct.get());
                System.out.println("Produsul a fost sters cu succes");
            } else {
                System.out.println("Product not found. Revenire la meniul principal");
            }
        } else {
            System.out.println("Revenire la meniul principal");
        }
        actionsForSuccessfullyAuthenticatedUser();

    }

    private static void menuDecisionThreeUpdateProduct() {

        //afisare lista produse
        ProductRepositoryImpl productsFromDB = ProductRepositoryImpl.getInstance();
        System.out.println(productsFromDB.findAll());

        //selectare produs
        Scanner scanner = ScannerHelper.getScanner();
        System.out.println("Selectati produsul dupa id");
        int productId = scanner.nextInt();
        scanner.nextLine();

        Optional<Product> productOptional = ProductRepositoryImpl.getInstance().findById(productId);
        if (productOptional.isPresent()) {
            Product productEntity = productOptional.get();

            ProductDTO productDTO = ProductMapper.mapEntityToDto(productEntity);

            //introducere modificari
            System.out.println("introdu nume");
            productDTO.setName(scanner.nextLine());

            System.out.println("Introdu cantitate");
            productDTO.setQuantity(scanner.nextFloat());
            scanner.nextLine();

            System.out.println("Introdu data de expirare");
            productDTO.setExpiryDate(scanner.nextLine());

            System.out.println("Introdu pret");
            productDTO.setPrice(scanner.nextFloat());
            scanner.nextLine();

            System.out.println("Introdu categorie");
            productDTO.setProductCategory(ProductCategory.valueOf(scanner.nextLine()));

            // update produs nou
            ProductRepositoryImpl.getInstance().update(ProductMapper.mapDtoToEntity(productDTO));

            // display all products
            System.out.println(ProductRepositoryImpl.getInstance().findAll());
            System.out.println("Produsul a fost modificat redirecting to main menu");

        } else {
            System.out.println("Product not found... redirecting to main menu");
        }
        actionsForSuccessfullyAuthenticatedUser();
    }

    private static void menuDecisionTwoFindProductByName() {
        Scanner scanner = ScannerHelper.getScanner();
        System.out.println("Introduceti numele produsului");

        ProductRepositoryImpl productsFromDB = ProductRepositoryImpl.getInstance();
        Optional<Product> foundProduct = productsFromDB.findByName(scanner.nextLine());

        if (foundProduct.isPresent()) {
            System.out.println(foundProduct.get());
        } else {
            System.out.println("Product not found... redirecting to main menu");
            actionsForSuccessfullyAuthenticatedUser();
        }

    }

    private static void menuDecisionOneFindAllProducts() {
        ProductRepository productRepository = ProductRepositoryImpl.getInstance();
        Optional<List<Product>> optionalProductList = productRepository.findAll();
        System.out.println(optionalProductList);

        System.out.println("Select product by id to be added in cart...");
        Scanner scanner = ScannerHelper.getScanner();
        int producId = scanner.nextInt();
        Optional<Product> productById = productRepository.findById(producId);
        if (productById.isPresent() && productById.get().getQuantity() > 0) {
            Product product = productById.get();
            UserSession instance = UserSession.getInstance();
            UserDTO userSessionDTO = instance.getUserSessionDTO();
            UserProduct userProduct = new UserProduct();
            userProduct.setName(product.getName());
            System.out.println("Introdu cantitatea dorita");
            float productQuantity = scanner.nextFloat();
            if (product.getQuantity() >= productQuantity) {
                userProduct.setQuantity(productQuantity);
                userSessionDTO.getUserCart().add(userProduct);

                product.setQuantity(product.getQuantity() - productQuantity);
                System.out.println("Produsul va fi actualizat");
                productRepository.update(product);

                actionsForSuccessfullyAuthenticatedUser();
            } else {
                System.out.println("Cantitate incorecta. Revenire la afisarea produselor");
                menuDecisionOneFindAllProducts();
            }

        } else {
            System.out.println("Produs incorect sau cantitate insuficienta.");
            menuDecisionOneFindAllProducts();
        }
    }

    private static int getLoggedUserDecision() {
        // todo add condition number to this list
        List<Integer> loginCondition = Arrays.asList(0, 1, 2, 3, 4, 5, 6);
        return getDecisionByConditions(loginCondition);
    }

    private static int getUserDecision() {
        System.out.println("Tastati 1 pentru a intra in cont");
        System.out.println("Tastati 2 pentru a crea un cont nou");
        List<Integer> loginCondition = Arrays.asList(1, 2);
        return getDecisionByConditions(loginCondition);
    }

    private static int getDecisionByConditions(List<Integer> conditions) {
        Scanner scanner = ScannerHelper.getScanner();
        int userDecision = 0;
        while (!conditions.contains(userDecision)) {
            try {
                userDecision = scanner.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Va rugam sa introduceti o optiune valida");
                userDecision = 0;
                scanner.nextLine();
            }
        }

        return userDecision;
    }

    private static boolean isExistingUser() {
        Scanner scanner = ScannerHelper.getScanner();
        scanner.nextLine();
        System.out.println("Introduceti numele utilizatorului");
        String userName = scanner.nextLine();
        System.out.println("Introduceti parola");
        String password = scanner.nextLine();

        // Apelam repository sa vedem daca userul si parola exista sau nu in baza de date
        UserRepository userRepository = UserRepositoryImpl.getInstance();
        Optional<User> optionalUser = userRepository.findByName(userName);
        if (optionalUser.isPresent()) {
            User foundUser = optionalUser.get();
            return checkPassword(password, foundUser);
        } else {
            System.out.println(String.format("Userul cu numele %s nu exista", userName));
            return false;
        }

    }

    private static boolean checkPassword(String password, User foundUser) {
        int counter = 0;
        Scanner scanner = ScannerHelper.getScanner();
        while (counter < 3) {
            UserSession userSession = UserSession.getInstance();
            UserDTO userDTO = UserMapper.mapEntityToDto(foundUser);
            if (password.equals(foundUser.getPassword())) {
                System.out.println("Autentificare cu succes!");
                userSession.setUserSessionDTO(userDTO);
                // todo set logged = true
                return true;
            } else {
                counter++;
                System.out.println("Parola este gresita! Reintroduce parola!");
                password = scanner.nextLine();
                if (password.equals(foundUser.getPassword())) {
                    System.out.println("Autentificare cu succes!");
                    userSession.setUserSessionDTO(userDTO);
                    // todo set logged = true
                    return true;
                }
            }
        }
        System.out.println("Acces blocat");
        return false;
    }

    private static void displayMenu() {
        System.out.println("1-Afisare produse existente");
        System.out.println("2-Cautare produs dupa nume");
        System.out.println("3-Modificare produs");
        System.out.println("4-Stergere produs");
        System.out.println("5-Adaugare produs");
        System.out.println("6-Cautare produs dupa pret");
        System.out.println("*-Revenire la meniul principal");

    }

    private static void createNewAccount() {
        Scanner scanner = ScannerHelper.getScanner();

        User user = new User();
        System.out.println("Set username");
        user.setUsername(scanner.nextLine());

        System.out.println("Set password");
        user.setPassword(scanner.nextLine());

        System.out.println("Set oras");
        user.setOras(scanner.nextLine());

        System.out.println("Set varsta");
        user.setVarsta(scanner.nextInt());

        try {
            System.out.println("Set Rol: ADMIN / USER");
            user.setRol(UserRol.valueOf(scanner.nextLine()));
        } catch (Exception e) {
            user.setRol(UserRol.USER);
        }

        UserRepository userRepository = UserRepositoryImpl.getInstance();
        userRepository.create(user);
        System.out.println("Userul: " + user + " a fost creat!");
    }
}
