package dto;

public class UserProduct {
    private String name;
    private float quantity;

    public UserProduct() {

    }

    public UserProduct(String name, float quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "UserProduct{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
