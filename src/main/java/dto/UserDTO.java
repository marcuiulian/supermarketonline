package dto;

import enums.UserRol;

import java.util.ArrayList;
import java.util.List;


public class UserDTO {

    /*todo add boolean for logged*/
    private Integer id;
    private String username;
    private String password;
    private Integer varsta;
    private String oras;
    private UserRol rol;
    private List<UserProduct> userCart = new ArrayList<>(0);


    public UserDTO() {
    }

    public UserDTO(Integer id, String username, String password, Integer varsta, String oras, UserRol rol) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.varsta = varsta;
        this.oras = oras;
        this.rol = rol;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getVarsta() {
        return varsta;
    }

    public void setVarsta(Integer varsta) {
        this.varsta = varsta;
    }

    public String getOras() {
        return oras;
    }

    public void setOras(String oras) {
        this.oras = oras;
    }

    public UserRol getRol() {
        return rol;
    }

    public void setRol(UserRol rol) {
        this.rol = rol;
    }

    public List<UserProduct> getUserCart() {
        return userCart;
    }

    public void setUserCart(List<UserProduct> userCart) {
        this.userCart = userCart;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", varsta=" + varsta +
                ", oras='" + oras + '\'' +
                ", rol=" + rol +
                ", userCart=" + userCart +
                '}';
    }
}
