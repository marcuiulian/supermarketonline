package config;

import dto.UserDTO;

public class UserSession {
    private UserDTO userSessionDTO;

    private static UserSession instance;

    private UserSession(){

    }
    public static UserSession getInstance(){
        if(instance == null){
            instance = new UserSession();
        }
        return instance;
    }

    public UserDTO getUserSessionDTO() {
        return userSessionDTO;
    }

    public void setUserSessionDTO(UserDTO userSessionDTO) {
        this.userSessionDTO = userSessionDTO;
    }
}
