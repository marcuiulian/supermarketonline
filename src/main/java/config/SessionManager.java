package config;

import config.AbstractSessionManager;
import entities.Product;
import entities.User;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SessionManager extends AbstractSessionManager {

    private static final SessionManager INSTANCE = new SessionManager();
    private static final String DATABASE_NAME = "supermarket_online";

    private SessionManager() {

    }

    public static SessionManager get() {
        return INSTANCE;
    }

    public static SessionFactory getSessionFactory() {
        return get().getSessionFactory(DATABASE_NAME);
    }

    @Override
    protected void setAnnotatedClasses(Configuration configuration) {
        configuration.addAnnotatedClass(User.class);
        configuration.addAnnotatedClass(Product.class);

    }


}
