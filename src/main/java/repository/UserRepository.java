package repository;

import entities.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    void create(User user);

    void delete(User user);

    void update(User user);

    Optional<User> findByName(String username);

    Optional<List<User>> findAll ();


}
