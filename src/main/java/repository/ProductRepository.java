package repository;

import entities.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepository {
    void create(Product product);
    
    void delete(Product product);
    
    void update(Product product);
    
    Optional<Product> findByName(String name);

    Optional<Product> findById(Integer id);
    
    Optional<List<Product>> findAll();

    Optional<List<Product>> findByMinPrice(float minPrice);
}
