package repository;

import config.SessionManager;
import entities.Product;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;


public class ProductRepositoryImpl implements ProductRepository {
    private static ProductRepositoryImpl instance;

    private ProductRepositoryImpl() {
    }

    public static ProductRepositoryImpl getInstance() {
        if (null == instance) {
            instance = new ProductRepositoryImpl();
        }
        return instance;
    }

    @Override
    public void create(Product product) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.save(product);

            transaction.commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void delete(Product product) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.delete(product);

            transaction.commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public void update(Product product) {
        Transaction transaction = null;
        Session session = null;
        try {
            session = SessionManager.getSessionFactory().openSession();
            transaction = session.beginTransaction();

            session.update(product);

            transaction.commit();
            session.close();

        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            if (session != null) {
                session.close();
            }
        }
    }

    @Override
    public Optional<Product> findByName(String name) {
        try {
            Session session = SessionManager.getSessionFactory().openSession();

            Product product = (Product) session.createQuery("from Product where nume:=nume")
                    .setString("nume", name)
                    .uniqueResult();

            session.close();
            return Optional.ofNullable(product);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<Product> findById(Integer id) {
        try {
            Session session = SessionManager.getSessionFactory().openSession();
            Product product = session.find(Product.class, id);
            session.close();
            return Optional.ofNullable(product);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }

    }


    @Override
    public Optional<List<Product>> findAll() {

        try {
            Session session = SessionManager.getSessionFactory().openSession();
            List<Product> product = session.createQuery("from Product", Product.class).list();
            session.close();
            return Optional.ofNullable(product);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    @Override
    public Optional<List<Product>> findByMinPrice(float minPrice) {

        try {
            Session session = SessionManager.getSessionFactory().openSession();

            List<Product> productList = (List<Product>) session.createQuery("from Product where price>=:pret")
                    .setParameter("pret", minPrice)
                    .list();

            session.close();
            return Optional.ofNullable(productList);
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        }

    }
}
