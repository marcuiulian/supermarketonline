package utils;

import java.util.Scanner;

public class ScannerHelper  {
    private static Scanner scanner;
    private ScannerHelper(){
    }
    public static Scanner getScanner(){
        if(scanner==null){
            scanner=new Scanner(System.in);
        }
        return scanner;
    }

}
