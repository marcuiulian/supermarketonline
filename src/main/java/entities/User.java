package entities;

import enums.UserRol;

import javax.persistence.*;

@Entity
@Table(name = "user", schema = "supermarket_online")
public class User {

    /*todo add boolean for logged, also add to SQL file logged COLUMN*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "varsta")
    private Integer varsta;

    @Column(name = "oras")
    private String oras;

    @Enumerated(EnumType.STRING)
    @Column(name = "rol")
    private UserRol rol;


    public User() {

    }

    public User(String username, String password, Integer varsta, String oras, UserRol rol) {
        this.username = username;
        this.password = password;
        this.varsta = varsta;
        this.oras = oras;
        this.rol = rol;
    }

    public User(Integer id, String username, String password, Integer varsta, String oras, UserRol rol) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.varsta = varsta;
        this.oras = oras;
        this.rol = rol;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getVarsta() {
        return varsta;
    }

    public void setVarsta(Integer varsta) {
        this.varsta = varsta;
    }

    public String getOras() {
        return oras;
    }

    public void setOras(String oras) {
        this.oras = oras;
    }

    public UserRol getRol() {
        return rol;
    }

    public void setRol(UserRol rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", parola='" + password + '\'' +
                ", varsta=" + varsta +
                ", oras='" + oras + '\'' +
                ", rol='" + rol + '\'' +
                '}';
    }
}
