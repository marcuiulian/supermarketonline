package entities;

import javax.persistence.*;

import enums.ProductCategory;

@Entity
@Table(name = "product", schema = "supermarket_online")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Integer id;
    
    @Column(name = "nume")
    private String name;
    
    @Column(name = "cantitate")
    private float quantity;
    
    @Column(name = "data_exp")
    private String expiryDate;
    
    @Column(name = "pret")
    private float price;
    
    @Column(name = "categorie")
    @Enumerated(value = EnumType.STRING)
    private ProductCategory productCategory;
    
    public Product(){
    
    }
    
    public Product(String name, float quantity, String expiryDate, float price, ProductCategory productCategory) {
        this.name = name;
        this.quantity = quantity;
        this.expiryDate = expiryDate;
        this.price = price;
        this.productCategory = productCategory;
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public float getQuantity() {
        return quantity;
    }
    
    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }
    
    public String getExpiryDate() {
        return expiryDate;
    }
    
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }
    
    public float getPrice() {
        return price;
    }
    
    public void setPrice(float price) {
        this.price = price;
    }
    
    public ProductCategory getProductCategory() {
        return productCategory;
    }
    
    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }
    
    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", expiryDate='" + expiryDate + '\'' +
                ", price=" + price +
                ", productCategory=" + productCategory +
                '}';
    }
}
